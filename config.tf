# specific alias provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      configuration_aliases = [ aws.us-east-1 ]
    }
  }
}

# retrieve dns zone
data "aws_route53_zone" "cf" {
  name = var.bucket_zone
}
