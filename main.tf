# define origin access control
resource "aws_cloudfront_origin_access_control" "cf" {
  name                              = var.bucket_name
  description                       = format("Access to %s", var.bucket_name)
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}

# define bucket policy
data "aws_iam_policy_document" "cf" {
  statement {
    sid       = "CloudFrontReadAccess"
    effect    = "Allow"
    actions   = ["s3:GetObject"]
    resources = ["${var.bucket_arn}/*"]
    principals {
      type        = "Service"
      identifiers = ["cloudfront.amazonaws.com"]
    }
    condition {
      test     = "StringEquals"
      variable = "AWS:SourceArn"
      values   = [aws_cloudfront_distribution.cf.arn]
    }
  }
}

# set bucket policy
resource "aws_s3_bucket_policy" "cf" {
  bucket = var.bucket_name
  policy = data.aws_iam_policy_document.cf.json
  count  = (var.force_policy ? 1 : 0)
}

# define certificate for distribution
resource "aws_acm_certificate" "cf" {
  provider          = aws.us-east-1
  domain_name       = var.bucket_name
  validation_method = "DNS"
  tags              = var.tags
  lifecycle {
    create_before_destroy = true
  }
}

# set dns record for cert validation
locals {
  validation_domains = [
    for k, v in try(aws_acm_certificate.cf.domain_validation_options, null) : tomap(v)
  ]
}
resource "aws_route53_record" "dvr" {
  allow_overwrite = true
  name            = element(local.validation_domains, 0)["resource_record_name"]
  type            = element(local.validation_domains, 0)["resource_record_type"]
  records         = [element(local.validation_domains, 0)["resource_record_value"]]
  ttl             = 60
  zone_id         = data.aws_route53_zone.cf.zone_id
  depends_on      = [aws_acm_certificate.cf]
}

# validate certificate
resource "aws_acm_certificate_validation" "cf" {
  provider                = aws.us-east-1
  certificate_arn         = aws_acm_certificate.cf.arn
  validation_record_fqdns = [aws_route53_record.dvr.fqdn]
  timeouts {
    create = "2m"
  }
}

# define cloudfront distribution
resource "aws_cloudfront_distribution" "cf" {
  aliases          = [var.bucket_name]
  price_class      = "PriceClass_100"
  is_ipv6_enabled  = true
  enabled          = true
  tags             = var.tags
  origin {
    domain_name              = var.bucket_rdn
    origin_id                = format("s3.%s", var.bucket_name)
    origin_path              = var.bucket_path
    origin_access_control_id = aws_cloudfront_origin_access_control.cf.id
  }
  default_cache_behavior {
    allowed_methods          = ["GET", "HEAD", "OPTIONS"]
    cached_methods           = ["GET", "HEAD"]
    target_origin_id         = format("s3.%s", var.bucket_name)
    viewer_protocol_policy   = "https-only"
    cache_policy_id          = var.cache_policy_id
    origin_request_policy_id = var.origin_policy_id
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate_validation.cf.certificate_arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2021"
  }
}

# define dns alias
resource "aws_route53_record" "cf" {
  zone_id = data.aws_route53_zone.cf.zone_id
  name    = var.bucket_name
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.cf.domain_name
    zone_id                = aws_cloudfront_distribution.cf.hosted_zone_id
    evaluate_target_health = false
  }
}
