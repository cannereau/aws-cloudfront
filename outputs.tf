output "distribution" {
  value       = aws_cloudfront_distribution.cf.id
  description = "CloudFront Distribution ID"
}

output "origin_access" {
  value       = aws_cloudfront_origin_access_control.cf.id
  description = "Origin Access Control ID"
}
