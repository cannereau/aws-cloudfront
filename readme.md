# AWS CloudFront

This is a Terraform module to build :
- AWS CloudFront distribution with S3 origin

**WARNING** : setting *force_policy* to *true* will replace the S3 bucket policy !

## Required provider :
- **aws.us-east-1** : needed to create custom CloudFront certificate

## Input parameters :
- **bucket_name** : Bucket's name
- **bucket_arn** : Bucket's ARN
- **bucket_rdn** : Bucket's region-specific domain name
- **bucket_zone** : Bucket's DNS zone
- **bucket_path** : Bucket's origin path (default value : "")
- **cache_policy_id** : Cache policy ID (default value : "Managed-CachingOptimized")
- **origin_policy_id** : Origin request policy ID (default value : "Managed-CORS-S3Origin")
- **force_policy** : Enforce CloudFront Origin Identity to access S3 objects (default value : false)
- **tags** : Application's tags

## Output values :
- **distribution** : CloudFront Distribution ID
- **origin_access** : Origin Access Control ID
