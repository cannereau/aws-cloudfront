variable "bucket_name" {
  type        = string
  description = "Bucket's name"
}

variable "bucket_arn" {
  type        = string
  description = "Bucket's ARN"
}

variable "bucket_rdn" {
  type        = string
  description = "Bucket's region-specific domain name"
}

variable "bucket_zone" {
  type        = string
  description = "Bucket's DNS zone"
}

variable "bucket_path" {
  type        = string
  default     = ""
  description = "Bucket's origin path"
}

variable "cache_policy_id" {
  type        = string
  default     = "658327ea-f89d-4fab-a63d-7e88639e58f6"
  description = "Cache policy ID"
}

variable "origin_policy_id" {
  type        = string
  default     = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
  description = "Origin request policy ID"
}

variable "force_policy" {
  type        = bool
  default     = false
  description = "Enforce S3 policy for CloudFront"
}

variable "tags" {
  type        = map(string)
  description = "Application's tags"
}
